import dotenv from 'dotenv'
import Binance from 'binance-api-node'

dotenv.config()

export default Binance({
  apiKey: process.env.API_KEY,
  apiSecret: process.env.SECRET_KEY,
})
