import binance from '../../../config/binance'

export default async function handler({query: {symbol}}, res) {
  const pair = symbol.endsWith('BTC') ? symbol : 'BTC'
  const price = await binance.prices({symbol: pair})
  console.log(price)
  res.status(200).send(Number(price))
}
