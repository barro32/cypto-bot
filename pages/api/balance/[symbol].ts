import binance from '../../../config/binance'


export default async function handler({query: {symbol}}, res) {
  const { balances } = await binance.accountInfo()
  const balance = balances.find(b => b.asset === symbol)
  res.status(200).json({balance: balance.free})
}
