import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Balance from '../src/components/Balance'
import Buy from '../src/components/Buy'


export default function Home() {

  return (
    <div className={styles.container}>
      <Head>
        <title>Signals</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Balance symbol={'ETH'} />
        <Balance symbol={'BTC'} />
        <Buy />
      </main>
    </div>
  )
}
