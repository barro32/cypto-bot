import { useEffect } from 'react'
import useSWR from 'swr'

async function fetcher(url) {
  const res = await fetch(url)
  const data = await res.json()

  if (res.status !== 200) {
    throw new Error(data.message)
  }
  return data
}

export default function api(url: string) {
  return useSWR(() => `api/${url}`, fetcher)
}

export function apiEffect(url: string, setter: (val: any) => void, deps: any[]) {
  useEffect(() => {
    const {data, error} = useSWR(() => `api/${url}`, fetcher)
    setter(data || error)
  }, deps)
}
