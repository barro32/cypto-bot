import api from '../utils/api'

export default function Balance({symbol}) {

  const {data, error} = api(`balance/${symbol}`)

  if (error) return <div>{error.message}</div>
  if (!data) return <div>Loading...</div>

  return (
    <div>
      {symbol}: {data.balance}
    </div>
  )
}
