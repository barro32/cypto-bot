import { useState } from 'react'
import { apiEffect } from '../utils/api'

export default function Buy() {
  const [symbol, setSymbol] = useState('ETH')
  const [price, setPrice] = useState(0)

  // apiEffect(`market/${symbol}`, setPrice, [symbol])

  return (
    <>
      <label>
        Symbol
        <input type='text' value={symbol} onChange={e => setSymbol(e.target.value)} />
      </label>
      <label>
        Price
        <input type='number' value={price} onChange={e => setPrice(Number(e.target.value))} />
      </label>
      <label>
        Amount
        <input type='number' />
      </label>
      <label>
        Target #1
        <input type='number' />
      </label>
      <label>
        Target #2
        <input type='number' />
      </label>
      <label>
        Target #3
        <input type='number' />
      </label>
      <label>
        Target #4
        <input type='number' />
      </label>
    </>
  )
}
